#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <semaphore.h>
#include <math.h>
#include <string.h>
#include <time.h>

#define TIME_TO_CROSS 7000

#define LEFT 0
#define RIGHT 1

#define PLAYING 0
#define TIRED 1

typedef struct x {
	int id;
	int side;
} Child;

Child *children;
sem_t *mutex;

int string_to_int(char *string);
void *child(void *index);

int main(int argc, char *argv[]){

	int t_child = string_to_int(argv[1]);
	int index;
	mutex = malloc(sizeof(sem_t));
	children = malloc(t_child * sizeof(Child));
	pthread_t *children_thread = malloc(t_child * sizeof(pthread_t));
	
	for(index = 0; index < t_child; index ++) {
		children[index].id = index + 1;
		children[index].side = rand() % 2;
	}

  	sem_init(mutex, 0, 1);
	for(index = 0; index < t_child; index ++) {
    	pthread_create(&children_thread[index], NULL, child, &children[index].id);
	}
	
	for (index = 0; index < t_child; index++) {
		pthread_join(children_thread[index], NULL);
	}
	return 0;
}

void *child(void *index){
    int my_index = *((int *)index);
	int playing_time = rand() % 10000;
	int die = rand() % 5 + 1;
	printf("Oi eu sou a criança %d\n", my_index);
	
	while(die--) {
		printf("%d: Estou do lado %s\n", my_index, children[my_index-1].side ? "Direito" : "Esquerdo");	
		printf("%d: Vou Brincar por %dms e depois tentarei atravesar\n", my_index, playing_time);	
		usleep(playing_time);
		
		sem_wait(mutex);
		printf("%d: Estou atravessando, HUEHUEHUE\n", my_index);
		children[my_index - 1].side = !children[my_index -1].side;
		usleep(TIME_TO_CROSS);
		sem_post(mutex);
	}

	printf("Eu a criança %d vou pra casa\n", my_index);

	return NULL;	
}

int string_to_int(char *string)
{
	int len = strlen(string);
	int result = 0;
	int index;

	for(index = len - 1; index >= 0; index --)
		result += ((string[index] - '0') * pow(10, len-index-1));

	return result;
}
