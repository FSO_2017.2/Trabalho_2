#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <unistd.h>
#include <time.h>

#define TIME_TO_CROSS 7000

#define LEFT 0
#define RIGHT 1

#define PLAYING 0
#define TIRED 1

typedef struct x {
	int id;
	int side;
} Child;

MPI_Win win;
void child(Child children);

int main(int argc, char *argv[]){
	int *mutex;

	MPI_Init(&argc, &argv);
	Child children;

	MPI_Comm_rank(MPI_COMM_WORLD, &children.id);
	srand(time(NULL) + children.id); 
	children.side = rand() % 2;

	MPI_Win_allocate_shared(((children.id) ? 0 : sizeof(int)), sizeof(int), MPI_INFO_NULL, MPI_COMM_WORLD, mutex, &win);

	child(children);
    
	MPI_Win_free(&win); 
	MPI_Finalize();
	return 0;
}

void child(Child children){
	int playing_time = rand() % 10000;
	int die = rand() % 5 + 1;
	printf("Oi eu sou a criança %d\n", children.id);
	
	while(die--) {
		printf("%d: Estou do lado %s\n", children.id, children.side ? "Direito" : "Esquerdo");	
		printf("%d: Vou Brincar por %dms e depois tentarei atravesar\n", children.id, playing_time);	
		usleep(playing_time);
		
		MPI_Win_sync(win);
		MPI_Win_lock_all(0, win);
		printf("%d: Estou atravessando, HUEHUEHUE\n", children.id);
		children.side = !children.side;
		usleep(TIME_TO_CROSS);
		MPI_Win_unlock_all(win);
	}

	printf("Eu a criança %d vou pra casa\n", children.id);
}
